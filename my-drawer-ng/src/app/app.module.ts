import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { TagsComponent } from "./tags-input/tags.component";
import { FuncionalidadService } from "./featured/funcionalidad/funcionalidad.service";
import { DetalleComponent } from './detalle/detalle.component';

import { EffectsModule} from "@ngrx/effects";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import {
    intializeNoticiasState,
    NoticiasEffects,
    NoticiasState,
    reducersNoticias
} from "./domain/noticias-state.model"
import { NoticiasService } from "./domain/noticias.service";

// import { InfodetallesComponent } from "./componentes/infodetalles/infodetalles.component";
// import { DetallesnotiComponent } from  "./componentes/detallesnoti/detallesnoti.component";

//redux init
//tslint:disable-next-line:interface-name
export interface AppState{
    noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducersNoticias
};

const reducersInitialState = {
    noticias: intializeNoticiasState()
};
//fin redux

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
        EffectsModule.forRoot([NoticiasEffects])
    ],

    providers: [NoticiasService],
    declarations: [
        AppComponent
    ],

    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
