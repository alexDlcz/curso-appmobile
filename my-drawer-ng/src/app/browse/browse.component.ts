import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as SocialShare from "nativescript-social-share";
import { fromFile, ImageSource } from "tns-core-modules/image-source";
import * as Toast from 'nativescript-toasts';
import { registerElement } from "nativescript-angular/element-registry";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
var gmaps = require("nativescript-google-maps-sdk");
@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    @ViewChild("MapView", { static: false }) mapView: ElementRef;
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    shareImage(): void {
        // let img = ImageSource.fromFile("res://icon");
        // SocialShare.shareImage(img);
        ImageSource.fromUrl("res://icon").then((image) => {
            SocialShare.shareImage(image);
        });
    }

    onMapReady(event): void {
        console.log("Map Ready");

        var mapView = event.object;
        var marker = new gmaps.Marker();

        marker.position = gmaps.Position.positionFromLatLng(-34.6037, -58.3817);
        marker.title = "Buenos Aires";
        marker.snippet = "Argentina";
        marker.userData = { index : 1 };
        mapView.addMarker(marker);
    }
}


