import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from "nativescript-angular/router";
import { Injectable } from "@angular/core";
import { DetalleComponent } from "./../detalle/detalle.component";
import { registerElement } from "nativescript-angular/element-registry";


@Component({
    selector: "Items",
    templateUrl: "./items.component.html",
  })
@Injectable()
export class ItemsComponent implements OnInit {
    private carros: Array<string> = [];
    constructor(private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
       this.carros[0] = 'Lamborghini';
       this.carros[1] = 'Bugatti';
       this.carros[2] = 'Ferrari'; 
     
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        console.dir(x);
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }

    carro() {
        return this.carros;
    }



}